# Kursbaustein "Gruppenaufgabe"  {: #course_element_group_tasks}


## Steckbrief

Name | Gruppenaufgabe
---------|----------
Icon | ![Gruppenaufgabe Icon](assets/task.png){ class=size24  }
Verfügbar seit | 
Funktionsgruppe | Wissensüberprüfung
Verwendungszweck | Abbildung komplexer Aufgabenworkflows mit konfigurierbaren Teilschritten wie Abgabe von Lösungsdokumenten, Feedback und Überarbeitungsschlaufe, Bereitstellung der Musterlösung und Bewertung
Bewertbar | ja
Spezialität / Hinweis | dem Kursbaustein Aufgabe ähnlich



Der Kursbaustein Gruppenaufgabe funktioniert größtenteils wie der Kursbaustein
["Aufgabe"](Course_Element_Task.de.md).

Punkte und Feedbacks werden in diesem Kursbaustein für die gesamte Gruppe erstellt, können aber individuell angepasst werden.

Folgende Unterschiede zum Kursbaustein Aufgabe sind zu beachten:

Bei der Erstellung der Gruppenaufgabe müssen im Tab "Workflow" eine oder mehrere Gruppen ausgewählt werden, denen die Aufgabenstellung zugewiesen wird. Nur diese Gruppen können die Aufgabe anschliessend einsehen und bearbeiten. Solange keine Gruppe zugewiesen wurde, ist der Kursbaustein bei geschlossenem Editor nicht sichtbar.

Bei geschlossenem Editor werden im Tab "Alle Teilnehmer" die zugeordneten Gruppen angezeigt, die dann ausgewählt und bewertet werden können. Im Rahmen des Bewertungsprozesses kann entschieden werden, ob die Bewertung für die gesamte Gruppe gilt oder ob die Bewertung nur für ein bestimmtes Gruppenmitglied gilt.
