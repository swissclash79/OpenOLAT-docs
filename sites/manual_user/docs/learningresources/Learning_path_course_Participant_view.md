## Learning path course - Participant view

A learning path course also differs from a traditional course for learners.
The biggest differences are in the progress indicator and the learning path
area. Also, learning path courses do not display tool icons to students for
orientation. The key areas are briefly highlighted below.

## Typical elements of a learning path course from the participants' point of view:

![](assets/Lernpfad_Kurs_Elemente.png)

  1.  **Progress bar** (learning path) in the course:  
Here the learners can see which elements they have already accessed or
completed and also which course areas are not yet accessible. Whether the display is visible can be set by the course owner in the course administration in the tab "Layout".

  2. "Learning path" link in the toolbar:  
Here, learners can access the **general overview** of their completion status
of all course elements with additional feedback information such as progress,
status, commitment, scheduled completion time, and any date information.
Course owners or coaches can see the progress of all course participants.  
![](assets/learning_path_overview.png)

  3. Visualized display of the **progress in percent**. Here, learners can see what percentage of the mandatory course elements they have already completed. If activated, the user's total points are also displayed here.
  4. Arrow buttons for navigation through the entire course. The user can use the arrows to navigate to the next course element. If certain requirements have to be met for the following course element that have not yet been met, e.g. confirmation by the user is missing or a test has not yet been completed, the message "This course element is not accessible." appears. Furthermore, "**Read confirmation**" appears, for example, if the user has to confirm that he has processed the corresponding module. An overview of possible confirmation criteria can be found [here](../learningresources/Learning_path_course_Course_editor.md).

  

The visualized display and infos provide learners
with a quick and continuous overview of their course progress and current
completion status.

The percentage of completion (3) can be based on the number of course elements
worked on or on the time units associated with the individual course elements. If
points are also to be displayed for the overall course evaluation (see
configuration in the "Settings" menu → "Assessment" tab), the corresponding
point value already achieved will also appear below the percentage display.

If the user has completed all the course elements defined by the coach, 100%
will be displayed. If new elements are added or deleted in the current course,
this is taken into account in the total and the learner's previous percentage
value is adjusted accordingly.

The coach can also configure [reminder e-mails](../learningresources/Course_Reminders.md) to be
sent when a certain percentage has been completed, e.g. anyone who has
completed less than 60% will be motivated once again to complete the remaining
work in the course.


