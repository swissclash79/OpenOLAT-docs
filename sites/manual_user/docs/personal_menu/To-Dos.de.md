# To-Dos

Die zu erledigenden Aufgaben (To-Dos) finden sich an mehreren Stellen in OpenOlat (z.B. innerhalb eines Projektes).<br> Im persönlichen Menü finden Sie alle Ihre **persönlichen** To-Dos zusammengefasst in einer **Übersicht**.

Sie können

* neue To-Dos erstellen (1)
* Ihre To-Dos nach Status sortiert anzeigen (2)
* durch Klick auf den kleinen Pfeil am Zeilenanfang die Details eines To-Dos aufklappen (3)
* die To-Dos bearbeiten (4)
* To-Dos löschen (Nach Selektion eines To-Dos in der ersten Spalte wird der Löschen-Button angezeigt) (5)

![to-do_example_v1_de.png](assets/to-do_example_v1_de.png){ class=" shadow lightbox" }


!!! note "Hinweis"

    Wenn Sie Ihre To-Dos statt im persönlichen Menü lieber in der Kopfzeile angezeigt haben möchten, können Sie die Menüoption vom persönlichen Menü dorthin verschieben. Die Einstellung dazu nehmen Sie vor unter<br>
    **Persönliches Menü > Einstellungen > Tab System > Abschnitt Persönliche Werkzeuge**<br>
    Alle Werkzeuge, die Sie hier markieren, werden statt im persönlichen Menü in der Kopfzeile rechts oben angezeigt und sind so schneller erreichbar.